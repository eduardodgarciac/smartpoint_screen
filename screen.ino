#include <Easyuino.h>
using Easyuino::Relay;
using Easyuino::DistanceMeter;

Relay powerRelay(8);
Relay hdmiRelay(7);
DistanceMeter proximitySensor(12, 11);

void setup() {
  Serial.begin(9600);
  
  powerRelay.begin();
  hdmiRelay.begin();
  proximitySensor.begin();

  delay(1500);
  powerRelay.turnOn();
  delay(500);
  powerRelay.turnOff();
}

void loop() {
  if (!hdmiRelay.isOn()) {
    delay(15000);
    hdmiRelay.turnOn();
  }

  proximitySensor.updateDistance();
  int distance = proximitySensor.getDistanceCentimeters();
  if (distance >= 0) {
    Serial.println(distance);
    delay(1000);
  }
}
