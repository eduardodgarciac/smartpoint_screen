#!/bin/bash

# Update & Upgrade
sudo apt update
sudo apt full-upgrade -y

# Install NodeJS
curl -sL https://deb.nodesource.com/setup_9.x | sudo -E bash -
sudo apt install nodejs -y

# Install Omxplayer
sudo apt install omxplayer -y

# Install Git
sudo apt-get install git -y
git config --global user.name "Eduardo García"
git config --global user.email eduardodgarciac@gmail.com
ssh-keygen -t rsa -b 4096 -C "eduardodgarciac@gmail.com"
cat ~/.ssh/id_rsa.pub

# Install libs to use usb-detection
sudo apt-get install build-essential libudev-dev -y

# Automount usb sticks
sudo apt-get install pmount -y
sudo nano /etc/udev/rules.d/usbstick.rules
ACTION=="add", KERNEL=="sd[a-z][0-9]", TAG+="systemd", ENV{SYSTEMD_WANTS}="usbstick-handler@%k"

sudo nano /lib/systemd/system/usbstick-handler@.service
[Unit]
Description=Mount USB sticks
BindsTo=dev-%i.device
After=dev-%i.device

[Service]
Type=oneshot
RemainAfterExit=yes
ExecStart=/usr/local/bin/automount %I
ExecStop=/usr/bin/pumount /dev/%I

sudo nano /usr/local/bin/automount
#!/bin/bash
if mountpoint -q /media/usb1
then
   if mountpoint -q /media/usb2
   then
      if mountpoint -q /media/usb3
      then
         if mointpoint -1 /media/usb4
         then
             echo "No mountpoints available!"
             #You can add more if you need
         else
             /usr/bin/pmount --umask 00 -noatime -r --sync $1 usb4
         fi
      else
         /usr/bin/pmount --umask 000 --noatime -r --sync $1 usb3
      fi
   else
      /usr/bin/pmount --umask 000 --noatime -r --sync $1 usb2
   fi
else
   /usr/bin/pmount --umask 000 --noatime -r --sync $1 usb1
fi

sudo chmod +x /usr/local/bin/automount

# Reboot
sudo reboot

# --------------------------------------------------------------------------------------------------------------------------

# Add autostart commands to raspberry
sudo nano /boot/cmdline.txt
quiet logo.nologo

# Add custom config to raspberry
sudo nano /boot/config.txt
# uncomment if hdmi display is not detected and composite is being output
hdmi_force_hotplug=1
# uncomment this if your display has a black border of unused pixels visible
# and your display can output without overscan
disable_overscan=1
# Hide splash screen
disable_splash=1
# Rotate screen to de left
display_rotate=3
#GPU Memory
gpu_mem=128

# Set custom settings to console and autostart software
nano ~/.profile
if [ "x${SSH_TTY}" = "x" ]; then
    export PS1=''
    setterm -foreground black -background white -cursor off -blink off -blank 0 -powersave off -clear all
    sh /home/pi/code/autostart.sh &
fi

# Reboot
sudo reboot