/*------------------------------------------------------------------------------
| Libs
'------------------------------------------------------------------------------*/
let _          = require('lodash'),
    shell      = require('shelljs'),
    request    = require('superagent'),
    drivelist  = require('drivelist'),
    fs         = require('fs'),
    omxManager = new (require('omx-manager'))(),
    omxPlayer  = null;

/*------------------------------------------------------------------------------
| Attributes
'------------------------------------------------------------------------------*/
let videosPath         = '/home/pi/videos',
    isSomethingOnFront = false,
    startVideosFrom    = null;

/*------------------------------------------------------------------------------
| Functions
'------------------------------------------------------------------------------*/
let data = {
    /*------------------------------------------------------------------------------
    | Data
    '------------------------------------------------------------------------------*/
    getUrl() {
        return 'https://smartpointcontrol.cl';
    },
    getMac() {
        let mac = shell.cat('/sys/class/net/eth0/address').stdout;
        return mac.replace("\n", '').toUpperCase();
    },
    getToken(auth) {
        return auth.token_type + ' ' + auth.access_token;
    },
    getAuth(url, mac) {
        return request.post(url + '/oauth/token').accept('json').send({
            grant_type: 'password',
            client_id: 1,
            client_secret: 'fQGapsQR3V1rcQP209NSyWOG36WjDlSsIdw1rmFn',
            username: mac,
            password: mac,
            scope: '*',
        });
    },
    getScreen(url, auth) {
        return request.get(url + '/api/user').accept('json').set('Authorization', data.getToken(auth));
    },

    /*------------------------------------------------------------------------------
    | Echo
    '------------------------------------------------------------------------------*/
    startEcho(url, auth) {
        global.window = {};
        global.io     = require('socket.io-client');
        global.echo   = new (require('laravel-echo'))({
            auth: {headers: {Authorization: data.getToken(auth)}},
            broadcaster: 'socket.io',
            host: url + ':6001'
        });
    },

    /*------------------------------------------------------------------------------
    | Proximity sensor
    '------------------------------------------------------------------------------*/
    startSensor() {
        let serialPort = require('serialport');

        serialPort.list((err, ports) => {
            if (_.find(ports, ['comName', '/dev/ttyACM0'])) {
                let port   = new serialPort('/dev/ttyACM0', {baudRate: 9600}),
                    parser = new serialPort.parsers.Readline({delimiter: '\r\n'});
                port.pipe(parser);
                parser.on('data', info => {
                    if (!isSomethingOnFront) {
                        let distance = _.toInteger(info);
                        if (distance <= 30) {
                            data.delaySensor();
                            data.stopVideos();
                        } else {
                            data.playVideosFromDrivelist();
                        }
                    }
                });
            }
        });
    },
    delaySensor() {
        isSomethingOnFront = true;
        startVideosFrom    = data.getCurrentVideo();
        _.delay(() => (isSomethingOnFront = false), 5000);
    },

    /*------------------------------------------------------------------------------
    | Videos
    '------------------------------------------------------------------------------*/
    playVideosFromDrivelist() {
        if (_.isNull(omxPlayer)) {
            drivelist.list((error, drives) => {
                if (error || drives.length < 2) {
                    data.playVideosFromPath(videosPath);
                } else {
                    data.playVideosFromPath(drives[0].mountpoints[0].path);
                }
            });
        }
    },
    playVideosFromPath(path) {
        let videos = fs.existsSync(path + '/list.json') ? _.intersection(JSON.parse(fs.readFileSync(path + '/list.json', 'utf8')), fs.readdirSync(path)) : fs.readdirSync(path);
        if (_.isArray(videos) && videos.length) {
            if (startVideosFrom) {
                let index = _.indexOf(videos, startVideosFrom);
                if (index > 0) {
                    videos = _.concat(_.slice(videos, index), _.slice(videos, 0, index - 1));
                }
                startVideosFrom = null;
            }
            omxManager.setVideosDirectory(path);
            omxPlayer = omxManager.create(videos, {'--loop': true, '--aspect-mode': 'fill'});
            omxPlayer.play();
        }
    },
    stopVideos() {
        if (!_.isNull(omxPlayer)) {
            omxPlayer.stop();
            omxPlayer = null;
        }
    },
    getCurrentVideo() {
        if (!_.isNull(omxPlayer)) {
            return omxPlayer.getStatus().current.replace(videosPath + '/', '');
        }
        return null;
    },

    /*------------------------------------------------------------------------------
    | Events
    '------------------------------------------------------------------------------*/
    setVideos(url, auth, remoteVideos) {
        data.stopVideos();
        let localVideos = fs.readdirSync(videosPath);

        _.each(_.difference(remoteVideos, localVideos), video => {
            let id     = video.substring(0, video.lastIndexOf('.')),
                path   = videosPath + '/' + video,
                stream = fs.createWriteStream(path, {flags: 'w+'});
            request.get(url + '/api/videos/' + id + '/download').set('Authorization', data.getToken(auth)).pipe(stream);
            stream.on('error', () => fs.unlinkSync(path));
        });

        _.each(_.difference(localVideos, remoteVideos), video => {
            let path = videosPath + '/' + video;
            fs.unlinkSync(path);
        });

        fs.writeFileSync(videosPath + '/list.json', JSON.stringify(remoteVideos), {flags: 'w+'});
        data.playVideosFromDrivelist();
    },
    setOrientation(value) {
        shell.exec('sudo sed -i "s/^display_rotate=.*$/display_rotate=' + _.toInteger(value) + '/" /boot/config.txt');
        data.reboot();
    },
    reboot() {
        shell.exec('sudo reboot');
    }
};

module.exports = data;