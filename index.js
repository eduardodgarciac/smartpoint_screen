let f   = require('./functions'),
    url = f.getUrl(),
    mac = f.getMac();

/*------------------------------------------------------------------------------
| Authenticate
'------------------------------------------------------------------------------*/
f.getAuth(url, mac).then(response => {
    let auth = response.body;

    /*------------------------------------------------------------------------------
    | Get the authenticated screen
    '------------------------------------------------------------------------------*/
    f.getScreen(url, auth).then(response => {
        let screen = response.body.user;

        /*------------------------------------------------------------------------------
        | Play videos
        '------------------------------------------------------------------------------*/
        f.playVideosFromDrivelist();

        /*------------------------------------------------------------------------------
        | Listen proximity sensor
        '------------------------------------------------------------------------------*/
        f.startSensor();

        /*------------------------------------------------------------------------------
        | Listen for socket events
        '------------------------------------------------------------------------------*/
        f.startEcho(url, auth);
        echo.join('user.' + screen.user_id + '.screens');
        echo.private('screen.' + screen.id).listen('ScreenVideos', event => {
            f.setVideos(url, auth, event.videos);
        }).listen('ScreenOrientation', event => {
            f.setOrientation(event.orientation);
        }).listen('ScreenReboot', event => {
            f.reboot();
        });

    }).catch(error => {
        f.playVideosFromDrivelist();
        f.startSensor();
    });

}).catch(error => {
    f.playVideosFromDrivelist();
    f.startSensor();
});

//Detectar cuando conectan un usb
/*var usbDetect = require('usb-detection');
usbDetect.startMonitoring();
// Detect add/insert
usbDetect.on('add', (device) => {
    console.log('add', device);
});
// Detect remove
usbDetect.on('remove', (device) => {
    console.log('remove', device);
});*/