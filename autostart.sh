#!/bin/bash

cd /home/pi/code
git pull --quiet
npm install --no-progress --quiet --silent
clear
node index.js